---
title: Workshop first post
date: 2017-10-06
---
# Voorbeeld post

Dit is een voorbeeld post voor de *workshop*.

## Done
Wat we hebben gedaan is:
1. Gitlab gemaakt
2. Hugo geforkt
3. Settings 

Wat we nog moeten doen:
* Thema wijzigen
* Nog meer steeings aanpassen
* _Iets wat cursief is_